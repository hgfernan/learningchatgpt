#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:11:24 2023

@author: ChatGPT-3
"""

import requests

def get_binance_orderbook(symbol, limit=100):
    url = f"https://api.binance.com/api/v3/depth?symbol={symbol}&limit={limit}"
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception if status code is not 200 OK
    except requests.exceptions.HTTPError as e:
        print(f"HTTP error occurred: {e}")
        return None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None
    data = response.json()
    return data

def get_highest_bid(orderbook):
    bids = orderbook["bids"]
    price = float(bids[0][0])
    amount = float(bids[0][1])
    return price, amount

def get_cheapest_ask(orderbook):
    asks = orderbook["asks"]
    price = float(asks[0][0])
    amount = float(asks[0][1])
    return price, amount

# Example usage
symbol = "BTCUSDT"
orderbook = get_binance_orderbook(symbol)
highest_bid_price, highest_bid_amount = get_highest_bid(orderbook)
cheapest_ask_price, cheapest_ask_amount = get_cheapest_ask(orderbook)

print(f"Highest bid price for {symbol}: {highest_bid_price}, amount: {highest_bid_amount}")
print(f"Cheapest ask price for {symbol}: {cheapest_ask_price}, amount: {cheapest_ask_amount}")
