#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 10:18:13 2023

@author: ChatGPT-3
"""

import requests

# Set up the request URL
url = 'https://api.binance.com/api/v3/depth'
params = {'symbol': 'BTCUSDT'}

# Fetch the order book data from the API
response = requests.get(url, params=params)
order_book = response.json()

# Extract the highest bid price and lowest ask price
highest_bid_price = float(order_book['bids'][0][0])
lowest_ask_price = float(order_book['asks'][0][0])

# Extract the volumes for each bid and ask
bid_volumes = [float(bid[1]) for bid in order_book['bids']]
ask_volumes = [float(ask[1]) for ask in order_book['asks']]

# Extract the amount of USDT for the highest bid and lowest ask prices
highest_bid_amount = highest_bid_price * bid_volumes[0]
lowest_ask_amount = lowest_ask_price * ask_volumes[0]

# Print the results
print(f'Highest bid price: {highest_bid_price:.2f}, Amount: {highest_bid_amount:.2f} USDT')
print(f'Lowest ask price: {lowest_ask_price:.2f}, Amount: {lowest_ask_amount:.2f} USDT')
