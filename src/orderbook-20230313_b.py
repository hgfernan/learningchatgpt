#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:11:24 2023

@author: ChatGPT-3
"""

import requests
from typing import List, Tuple, Union

def get_binance_orderbook(symbol: str, limit: int = 5) -> Tuple[int, Union[dict[str, Union[int, List[List[float]]]], None]]:
    try:
        url = f"https://api.binance.com/api/v3/depth?symbol={symbol}&limit={limit}"
        response = requests.get(url)
        response.raise_for_status()
        orderbook = response.json()
        return (0, orderbook)
    except requests.exceptions.HTTPError as http_err:
        return (http_err.response.status_code, None)
    except Exception:
        return (-1, None)

def get_highest_bid(orderbook: dict[str, Union[int, List[List[float]]]]) -> Tuple[float, float]:
    bids = orderbook["bids"]
    price = float(bids[0][0])
    amount = float(bids[0][1])
    return price, amount

def get_cheapest_ask(orderbook: dict[str, Union[int, List[List[float]]]]) -> Tuple[float, float]:
    asks = orderbook["asks"]
    price = float(asks[0][0])
    amount = float(asks[0][1])
    return price, amount

# Example usage
symbol = "BTCUSDT"
orderbook_code, orderbook = get_binance_orderbook(symbol)
if orderbook_code == 0:
    highest_bid_price, highest_bid_amount = get_highest_bid(orderbook)
    cheapest_ask_price, cheapest_ask_amount = get_cheapest_ask(orderbook)
    print(f"Highest bid price for {symbol}: {highest_bid_price}, amount: {highest_bid_amount}")
    print(f"Cheapest ask price for {symbol}: {cheapest_ask_price}, amount: {cheapest_ask_amount}")
else:
    print(f"Error getting order book for {symbol}. Return code: {orderbook_code}")
