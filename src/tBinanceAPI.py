#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 13:29:36 2023

@author: ChatGPT
"""

import sys
import time
import requests
# from typing import List, Tuple, Union, Optional
from typing import List, Tuple, Union

Orderbook = dict[str, Union[int, List[List[float]]]]

class BinanceAPI:
    def __init__(self, max_retries: int = 3, retry_delay: int = 1):
        self.max_retries = max_retries
        self.retry_delay = retry_delay
    
    def get_orderbook(self, symbol: str, limit: int = 5) -> Tuple[int, Orderbook]:
        for retry in range(self.max_retries + 1):
            try:
                url = f"https://api.binance.com/api/v3/depth?symbol={symbol}&limit={limit}"
                response = requests.get(url)
                response.raise_for_status()
                orderbook = response.json()
                return (0, orderbook)
            except requests.exceptions.HTTPError as http_err:
                return (http_err.response.status_code, {})
            except Exception:
                if retry == self.max_retries:
                    return (-1, {})
                else:
                    time.sleep(self.retry_delay)
    
    def get_highest_bid(self, orderbook: Orderbook) -> Tuple[float, float]:
        bids = orderbook["bids"]
        price = float(bids[0][0])
        amount = float(bids[0][1])
        return price, amount

    def get_cheapest_ask(self, orderbook: Orderbook) -> Tuple[float, float]:
        asks = orderbook["asks"]
        price = float(asks[0][0])
        amount = float(asks[0][1])
        return price, amount

def main(argv):
    api = BinanceAPI(max_retries=3, retry_delay=1)
    symbol = "BTCUSDT"
    error_code, orderbook = api.get_orderbook(symbol)
    if error_code == 0:
        highest_bid_price, highest_bid_amount = api.get_highest_bid(orderbook)
        cheapest_ask_price, cheapest_ask_amount = api.get_cheapest_ask(orderbook)
    
        print(f"Highest bid price for {symbol}: {highest_bid_price}, amount: {highest_bid_amount}")
        print(f"Cheapest ask price for {symbol}: {cheapest_ask_price}, amount: {cheapest_ask_amount}")
        return 0
    else:
        if error_code == -1:
            print("Internal error occurred.")
        else:
            print(f"HTTP error {error_code} occurred.")
            
        return error_code
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))
