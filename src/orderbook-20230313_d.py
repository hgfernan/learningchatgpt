#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:11:24 2023

@author: ChatGPT-3
"""

import requests
from typing import List, Tuple, Union

Orderbook = dict[ str : Union[int, List[List[float]]] ]

def get_binance_orderbook(symbol: str, limit: int = 5) -> Tuple[int, Orderbook]:
    try:
        url = f"https://api.binance.com/api/v3/depth?symbol={symbol}&limit={limit}"
        response = requests.get(url)
        response.raise_for_status()
        orderbook = response.json()
        return (0, orderbook)
    except requests.exceptions.HTTPError as http_err:
        return (http_err.response.status_code, {})
    except Exception:
        return (-1, {})

def get_highest_bid(orderbook: Orderbook) -> Tuple[float, float]:
    bids = orderbook["bids"]
    price = float(bids[0][0])
    amount = float(bids[0][1])
    return price, amount

def get_cheapest_ask(orderbook: Orderbook) -> Tuple[float, float]:
    asks = orderbook["asks"]
    price = float(asks[0][0])
    amount = float(asks[0][1])
    return price, amount

# Example usage
symbol = "BTCUSDT"
error_code, orderbook = get_binance_orderbook(symbol)
if error_code == 0:
    highest_bid_price, highest_bid_amount = get_highest_bid(orderbook)
    cheapest_ask_price, cheapest_ask_amount = get_cheapest_ask(orderbook)

    print(f"Highest bid price for {symbol}: {highest_bid_price}, amount: {highest_bid_amount}")
    print(f"Cheapest ask price for {symbol}: {cheapest_ask_price}, amount: {cheapest_ask_amount}")
else:
    if error_code == -1:
        print("Internal error occurred.")
    else:
        print(f"HTTP error {error_code} occurred.")
