#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 11:10:47 2023

@author: ChatGPT
"""

import requests
from time import sleep

def retry_get(url, retries=3, delay=1, backoff=2):
    """
    Retries a GET request a specified number of times before giving up.

    :param url: The URL to make the GET request to.
    :param retries: The number of times to retry the GET request before giving up.
    :param delay: The delay between each retry in seconds.
    :param backoff: The factor by which to increase the delay between retries.
    :return: The response from the final GET request.
    """
    attempt = 0
    while attempt <= retries:
        try:
            response = requests.get(url)
            response.raise_for_status()
            return response
        except (requests.exceptions.RequestException, ValueError):
            sleep(delay)
            delay *= backoff
            attempt += 1
    raise Exception(f"Could not get response from {url} after {retries} retries.")

# Example usage:
response = retry_get('https://api.binance.com/api/v3/depth?symbol=BTCUSDT&limit=5', retries=3)
orderbook = response.json()
print(orderbook)
