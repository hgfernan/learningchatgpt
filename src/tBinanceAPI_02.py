#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 13:36:20 2023

@author: ChatGPT
"""

import sys  # argv, exit()
import time # sleep()

from typing import List, Tuple, Union

import requests

Orderbook = dict[str, Union[int, List[List[float]]]]
Trade = Tuple[float, float, int, int, int]
OHLC = Tuple[int, float, float, float, float, float]

class BinanceAPI:
    def __init__(self, base_url: str = "https://api.binance.com/api/v3", retries: int = 3, retry_interval: int = 5):
        self.base_url = base_url
        self.retries = retries
        self.retry_interval = retry_interval
    
    def retry_get(self, endpoint: str, params: dict = None):
        for i in range(self.retries):
            try:
                response = requests.get(self.base_url + endpoint, params=params)
                response.raise_for_status()
                return response.json()
            except (requests.exceptions.HTTPError, requests.exceptions.ConnectionError) as e:
                if i < self.retries - 1:
                    time.sleep(self.retry_interval)
                else:
                    raise e
    
    def get_orderbook(self, symbol: str, limit: int = 5) -> Orderbook:
        endpoint = f"/depth?symbol={symbol}&limit={limit}"
        return self.retry_get(endpoint)
    
    def get_trades(self, symbol: str, limit: int = 5) -> List[Trade]:
        endpoint = f"/trades?symbol={symbol}&limit={limit}"
        return self.retry_get(endpoint)
    
    def get_ohlc(self, symbol: str, interval: str = "1m", limit: int = 5) -> List[OHLC]:
        endpoint = f"/klines?symbol={symbol}&interval={interval}&limit={limit}"
        klines = self.retry_get(endpoint)
        return [(int(k[0]), float(k[1]), float(k[2]), float(k[3]), float(k[4]), float(k[5])) for k in klines]

def main(argv : list[str]) -> int:
    binance = BinanceAPI()
    
    try:
        # Get the order book for BTCUSDT
        print('orderbook')
        orderbook = binance.get_orderbook("BTCUSDT", limit=10)
        print(f'{orderbook}')
        
        # Get the last 5 trades for BTCUSDT
        print('trades')
        trades = binance.get_trades("BTCUSDT", limit=5)
        print(f'{trades}')
        
        # Get the last 5 OHLC candles for BTCUSDT with 1-minute intervals
        print('OHLC')
        ohlc = binance.get_ohlc("BTCUSDT", interval="1m", limit=5)
        print(f'{ohlc}')
    
    except Exception as exc:
        print(f'{type(exc)} : {exc}')
    
    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
