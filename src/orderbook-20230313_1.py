#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 14 12:11:24 2023

@author: hilton
"""

import requests

def get_binance_orderbook(symbol):
    url = f"https://api.binance.com/api/v3/depth?symbol={symbol}&limit=5"
    response = requests.get(url)
    data = response.json()
    return data

def get_highest_bid_price(orderbook):
    bids = orderbook["bids"]
    return float(bids[0][0])

def get_cheapest_ask_price(orderbook):
    asks = orderbook["asks"]
    return float(asks[0][0])

# Example usage
symbol = "BTCUSDT"
orderbook = get_binance_orderbook(symbol)
highest_bid_price = get_highest_bid_price(orderbook)
cheapest_ask_price = get_cheapest_ask_price(orderbook)

print(f"Highest bid price for {symbol}: {highest_bid_price}")
print(f"Cheapest ask price for {symbol}: {cheapest_ask_price}")
