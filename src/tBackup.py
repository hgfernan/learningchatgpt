#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  8 15:22:06 2023

@author: ChatGPT
"""

import sqlite3

# Open the source database
source_conn = sqlite3.connect('source.db')
source_cursor = source_conn.cursor()

# Open the target database
target_conn = sqlite3.connect('target.db')
target_cursor = target_conn.cursor()

# Create a backup object and call its backup method to copy the source database to the target database
backup = sqlite3.backup(source_conn, target_conn)
backup.backup()

# Close the database connections
source_conn.close()
target_conn.close()

