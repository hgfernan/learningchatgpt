#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 17:29:03 2023

@author: ChatGPT
"""

def find_order_book(obs, t):
    for i, row in enumerate(obs):
        for j in range(len(row) - 1):
            if row[j] <= t <= row[j + 1]:
                return i, j  # Return both row and column indices
    return -1, -1  # Return -1 if no order book contains the trade value

# Example usage
obs = [[1, 2], [2, 3]]
t = 2.5

row, col = find_order_book(obs, t)
if row != -1 and col != -1:
    print(f"Trade value {t} is within the order book at row {row}, column {col}.")
else:
    print("No order book contains the trade value.")

"""
In this updated version, the find_order_book function uses nested loops to iterate over the rows and columns of the obs array. It checks each value within the order books to find a match with the trade value t. If a match is found, it returns the row and column indices. If no match is found, it returns -1 for both indices.

I apologize for any confusion caused by the previous oversight. Thank you for bringing it to my attention, and I appreciate your understanding.
"""
